﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Desafio_Xadrez.Entities
{
    class Tabuleiro
    {
        public int[,] Matriz { get; set; }
        public int[] Count { get; set; }

        public Tabuleiro()
        {
            Matriz = new int[8, 8] { { 4, 3, 2, 5, 6, 2, 3, 4} , 
                                     { 1, 1, 1, 1, 1, 1, 1, 1}, 
                                     { 0, 0, 0, 0, 0, 0, 0, 0}, 
                                     { 0, 0, 0, 0, 0, 0, 0, 0},
                                     { 0, 0, 0, 0, 0, 0, 0, 0}, 
                                     { 0, 0, 0, 0, 0, 0, 0, 0}, 
                                     { 1, 1, 1, 1, 1, 1, 1, 1}, 
                                     { 4, 3, 2, 5, 6, 2, 3, 4}};
            Count = new int[10];
        }


        public void MostrarTabuleiro()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Console.Write($"{Matriz[i, j]} ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        public void ContarPecasTabuleiro()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    while (Matriz[i, j] == 1)
                    {
                        Count[0]++;
                        break;
                    }
                    while (Matriz[i, j] == 2)
                    {
                        Count[1]++;
                        break;
                    }
                    while (Matriz[i, j] == 3)
                    {
                        Count[2]++;
                        break;
                    }
                    while (Matriz[i, j] == 4)
                    {
                        Count[3]++;
                        break;
                    }
                    while (Matriz[i, j] == 5)
                    {
                        Count[4]++;
                        break;
                    }
                    while (Matriz[i, j] == 6)
                    {
                        Count[5]++;
                        break;
                    }

                }
            }

            Console.WriteLine($"Peão: {Count[0]} Peça(s)");
            Console.WriteLine($"Bispo: {Count[1]} Peça(s)");
            Console.WriteLine($"Cavalo: {Count[2]} Peça(s)");
            Console.WriteLine($"Torre: {Count[3]} Peça(s)");
            Console.WriteLine($"Rainha: {Count[4]} Peça(s)");
            Console.WriteLine($"Rei: {Count[5]} Peça(s)");

        }
    }
}
