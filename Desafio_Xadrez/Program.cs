﻿using System;
using Desafio_Xadrez.Entities;
using System.Linq;

namespace Desafio_Xadrez
{
    class Program
    {
        static void Main(string[] args)
        {
            Tabuleiro T = new Tabuleiro();

            T.MostrarTabuleiro();

            T.ContarPecasTabuleiro();

        }
    }
}
